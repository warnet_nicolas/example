# Industrial Project - Front
 
## Requirements
* npm : `sudo apt-get install npm`
* node : `sudo apt-get install nodejs-legacy`

## Installation
* git clone https://bitbucket.org/warnet_nicolas/example.git
* cd example
* `npm install`
* `npm run build`
* `npm start`